# Event Registration

This extends the [Event](https://www.drupal.org/project/event] module to allow
registration for events.
