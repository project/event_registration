<?php

namespace Drupal\event_registration\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Registration entities.
 *
 * @ingroup event_registration
 */
class RegistrationDeleteForm extends ContentEntityDeleteForm {


}
