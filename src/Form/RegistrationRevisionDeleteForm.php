<?php

namespace Drupal\event_registration\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Registration revision.
 *
 * @ingroup event_registration
 */
class RegistrationRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Registration revision.
   *
   * @var \Drupal\event_registration\Entity\RegistrationInterface
   */
  protected $revision;

  /**
   * The Registration storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $registrationStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->registrationStorage = $container->get('entity_type.manager')->getStorage('event_registration');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_registration_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => \Drupal::service('date.formatter')->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.event_registration.version_history', [
      'event_registration' => $this->revision->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $event_registration_revision = NULL) {
    $this->revision = $this->RegistrationStorage->loadRevision($event_registration_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->RegistrationStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Registration: deleted %title revision %revision.', [
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Registration %title has been deleted.', [
      '%revision-date' => \Drupal::service('date.formatter')->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.event_registration.canonical',
       [
         'event_registration' => $this->revision->id(),
       ]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {event_registration_field_revision} WHERE id = :id', [
      ':id' => $this->revision->id(),
    ])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.event_registration.version_history',
         ['event_registration' => $this->revision->id()]
      );
    }
  }

}
