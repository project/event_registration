<?php

namespace Drupal\event_registration;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for event_registration.
 */
class RegistrationTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
